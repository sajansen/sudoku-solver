import logging

import solver
from sudoku import Sudoku

logger = logging.getLogger(__name__)


def init_sudoku_with_values(sudoku: Sudoku, value_matrix: list) -> None:
  sudoku.clear()

  for row, row_values in enumerate(value_matrix):
    for column, column_value in enumerate(row_values):
      if column_value is None:
        continue
      sudoku.matrix[row][column].value = column_value


def char_list_to_int_list(input_list: list) -> list:
  output_list = [None] * 9
  for i, char in enumerate(input_list):
    try:
      output_list[i] = int(char)
    except ValueError:
      pass
  return output_list


def string_to_int_list(string: str) -> list:
  string = string.replace('.', '')
  string = string.replace('|', '')
  string = string.replace('0', ' ')
  string = list(string)
  return char_list_to_int_list(string)


def read_sudoku() -> Sudoku:
  sudoku = Sudoku()
  input_rows = []
  input_matrix = []

  for row_index in range(0, 9):
    input_row = input("Insert row {}: ".format(row_index + 1))
    input_rows.append(input_row)

  for input_row in input_rows:
    input_list = string_to_int_list(input_row)
    input_matrix.append(input_list)

  init_sudoku_with_values(sudoku, input_matrix)
  return sudoku


def main() -> None:
  sudoku = read_sudoku()

  print("\n\n============   INITIAL   ============\n")
  sudoku.print()

  print("\n=====================================\n\n")
  print("              solving...")
  solver.solve(sudoku)

  print("\n\n============   SOLUTION   ===========\n")
  sudoku.print()
  print("\n=====================================\n")

  sudoku.print_extended()


if __name__ == '__main__':
  logging = logging.basicConfig(level=logging.DEBUG)
  logger.debug("Application started")
  main()
