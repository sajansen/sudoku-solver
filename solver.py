import logging

from sudoku import Sudoku, Cell
from utils import index_of_element_in_2darray, block_cells_as_list

logger = logging.getLogger(__name__)


def fill_cellnotes_with_all_values(sudoku: Sudoku):
  for row in sudoku.matrix:
    for cell in row:
      cell.notes = [1, 2, 3, 4, 5, 6, 7, 8, 9]


def existing_cells_in_block(sudoku: Sudoku, row_index: int, cell_index: int) -> list:
  block_start_row_index = 3 * (row_index // 3)
  block_start_column_index = 3 * (cell_index // 3)

  existing_cells = []
  for row in sudoku.matrix[block_start_row_index:block_start_row_index + 3]:
    for cell in row[block_start_column_index:block_start_column_index + 3]:
      if cell.value is None:
        continue
      existing_cells.append(cell.value)

  existing_cells.sort()
  return existing_cells


def existing_cells_in_row(sudoku: Sudoku, row_index: int) -> list:
  existing_cells = []

  for cell in sudoku.matrix[row_index]:
    if cell.value is None:
      continue
    existing_cells.append(cell.value)

  existing_cells.sort()
  return existing_cells


def existing_cells_in_column(sudoku: Sudoku, cell_index: int) -> list:
  existing_cells = []

  for row in sudoku.matrix:
    cell = row[cell_index]
    if cell.value is None:
      continue
    existing_cells.append(cell.value)

  existing_cells.sort()
  return existing_cells


def eliminate_known_cellnote_values(sudoku: Sudoku):
  for row_index, row in enumerate(sudoku.matrix):
    for cell_index, cell in enumerate(row):
      if cell.value is not None:
        cell.notes = []
        continue

      # Remove all notes from existing cells in the current block
      existings_cells = existing_cells_in_block(sudoku, row_index, cell_index)

      # Remove all notes from existing cells in the current row
      existings_cells += existing_cells_in_row(sudoku, row_index)

      # Remove all notes from existing cells in the current column
      existings_cells += existing_cells_in_column(sudoku, cell_index)

      cell.notes = [note for note in cell.notes if note not in existings_cells]
      cell.notes.sort()


def first_cell_with_one_note(sudoku: Sudoku) -> Cell:
  for row_index, row in enumerate(sudoku.matrix):
    for cell_index, cell in enumerate(row):
      if cell.value is not None:
        continue

      if len(cell.notes) > 1:
        continue

      if len(cell.notes) == 0:
        sudoku.print_extended()
        raise ValueError(
          "Cell ({},{}) doesn't has a value nor does it have notes!".format(row_index, cell_index))

      return cell
  return None


def single_note_number_in_cell_list(cells) -> (Cell, int):
  for number in range(1, 10):
    found_count = 0
    found_cell = None
    for cell in cells:
      if found_count > 1:
        continue

      if number not in cell.notes:
        continue

      found_count += 1
      found_cell = cell

    if found_count != 1:
      continue

    return found_cell, number
  return None, None


def single_note_number_in_block(block: list) -> (Cell, int):
  cells = block_cells_as_list(block)

  return single_note_number_in_cell_list(cells)


def single_note_number_in_rows(sudoku: Sudoku) -> (Cell, int):
  for row in sudoku.matrix:
    found_cell, number = single_note_number_in_cell_list(row)

    if found_cell is None:
      continue

    return found_cell, number
  return None, None


def single_note_number_in_columns(sudoku: Sudoku) -> (Cell, int):
  columns = sudoku.get_columns()

  for column in columns:
    found_cell, number = single_note_number_in_cell_list(column)

    if found_cell is None:
      continue

    return found_cell, number
  return None, None


def find_double_notes_in_block(block: list) -> (Cell, Cell):
  cells = block_cells_as_list(block)

  for cell in cells:
    if len(cell.notes) != 2:
      continue

    for other_cell in [c for c in cells if c is not cell]:
      if cell.notes != other_cell.notes:
        continue
      return cell, other_cell
  return None, None


def handle_double_notes_in_block(sudoku: Sudoku, block: list) -> bool:
  something_changed = False
  cells = block_cells_as_list(block)

  first_cell, second_cell = find_double_notes_in_block(block)
  if first_cell is None:
    return False

  double_notes = list(first_cell.notes)

  # Remove notes so we won't get confused when checking for changing notes later on
  first_cell.notes = []
  second_cell.notes = []

  # Remove these notes from the other cells in the block
  for cell in cells:
    if cell is first_cell or cell is second_cell:
      continue

    for note in double_notes:
      if note in cell.notes:
        cell.notes.remove(note)
        something_changed = True

  first_cell_row_index, first_cell_column_index = index_of_element_in_2darray(sudoku.matrix, first_cell)
  second_cell_row_index, second_cell_column_index = index_of_element_in_2darray(sudoku.matrix, second_cell)

  logger.debug("Eliminating for double-notes {} at ({},{}) and ({}, {})".format(double_notes, first_cell_row_index,
                                                                                first_cell_column_index,
                                                                                second_cell_row_index,
                                                                                second_cell_column_index))

  # Remove these notes from the other cells in the row
  if first_cell_row_index == second_cell_row_index:
    something_changed |= sudoku.remove_notes_from_row(first_cell_row_index, double_notes)

  # Remove these notes from the other cells in the column
  if first_cell_column_index == second_cell_column_index:
    something_changed |= sudoku.remove_notes_from_column(first_cell_column_index, double_notes)

  first_cell.notes = list(double_notes)
  second_cell.notes = list(double_notes)
  return something_changed


# Returns a list of matching cells which all contains the same number (2nd argument) and are distributed in the same
# row or column (third argument holds the (row,column) index)
def find_double_note_number_in_block(block: list, number: int) -> (list, tuple):
  found_cells_and_positions = []

  for row_index, row in enumerate(block):
    for cell_index, cell in enumerate(row):
      if number not in cell.notes:
        continue

      found_cells_and_positions.append({"cell": cell,
                                        "row": row_index,
                                        "column": cell_index})

  if not found_cells_and_positions:
    return None, None

  same_row = True
  same_column = True
  for cell_and_position in found_cells_and_positions:
    for other_cell_and_position in [c for c in found_cells_and_positions if c is not cell_and_position]:
      same_row &= other_cell_and_position['row'] == cell_and_position['row']
      same_column &= other_cell_and_position['column'] == cell_and_position['column']

  if not same_row and not same_column:
    return None, None

  return [cell_and_position['cell'] for cell_and_position in found_cells_and_positions], \
         (found_cells_and_positions[0]['row'] if same_row else None,
          found_cells_and_positions[0]['column'] if same_column else None)


def handle_double_note_number_in_block_and_row_or_column(sudoku: Sudoku, block: list, number: int) -> bool:
  something_changed = False

  found_cells, block_row_column = find_double_note_number_in_block(block, number)
  if found_cells is None:
    return False

  first_cell_row_index, first_cell_column_index = index_of_element_in_2darray(sudoku.matrix, found_cells[0])
  sudoku_row_column = (first_cell_row_index if block_row_column[0] is not None else None,
                       first_cell_column_index if block_row_column[1] is not None else None)

  logger.debug("Eliminating for predicted-row-column-number {} in row {} and column {}".format(number,
                                                                                               sudoku_row_column[0],
                                                                                               sudoku_row_column[1]))

  # Backup found cells
  found_cells_backup_notes = [cell.notes for cell in found_cells]
  for cell in found_cells:
    cell.notes = []

  # Remove these notes from the other cells in the row
  if sudoku_row_column[0] is not None:
    something_changed |= sudoku.remove_notes_from_row(sudoku_row_column[0], [number])

  # Remove these notes from the other cells in the column
  if sudoku_row_column[1] is not None:
    something_changed |= sudoku.remove_notes_from_column(sudoku_row_column[1], [number])

  # Backup found cells
  for i, cell in enumerate(found_cells):
    cell.notes = found_cells_backup_notes[i]

  return something_changed


def handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block):
  something_changed = False
  for number in range(1, 10):
    if handle_double_note_number_in_block_and_row_or_column(sudoku, block, number):
      something_changed = True
  return something_changed


def find_xy_wing_cells_in_cells(cells: list) -> (list, list):
  xywing_cells = [None] * 3
  xywing_notes = [None] * 3

  # First look for a cell containing only two notes
  for first_cell in cells:
    if len(first_cell.notes) != 2:
      continue

    xywing_cells[0] = first_cell

    xywing_notes[0] = xywing_cells[0].notes[0]
    xywing_notes[1] = xywing_cells[0].notes[1]

    # Than, look for a cell, also containing only two notes, but only one of them must be the same as
    # one of the first cell
    for second_cell in cells:
      if len(second_cell.notes) != 2:
        continue

      if second_cell in xywing_cells:
        continue

      if xywing_cells[0].notes[0] in second_cell.notes:
        if xywing_cells[0].notes[1] in second_cell.notes:
          continue
        xywing_notes[0] = xywing_cells[0].notes[0]
        xywing_notes[1] = xywing_cells[0].notes[1]
      elif xywing_cells[0].notes[1] in second_cell.notes:
        if xywing_cells[0].notes[0] in second_cell.notes:
          continue
        xywing_notes[0] = xywing_cells[0].notes[1]
        xywing_notes[1] = xywing_cells[0].notes[0]
      else:
        continue

      xywing_cells[1] = second_cell

      second_cell_notes = list(xywing_cells[1].notes)
      second_cell_notes.remove(xywing_notes[0])
      xywing_notes[2] = second_cell_notes[0]

      # Next, look for another cell containing only one note of the previous found cells
      for third_cell in cells:
        if third_cell in [first_cell, second_cell]:
          continue

        if xywing_notes[0] in third_cell.notes:
          continue

        if xywing_notes[1] not in third_cell.notes:
          continue

        if xywing_notes[2] not in third_cell.notes:
          continue

        xywing_cells[2] = third_cell

        xywing_notes.sort()
        return xywing_cells, xywing_notes

  return None, None


def handle_xy_wing_in_cells(cells: list) -> bool:
  something_changed = False

  xywing_cells, xywing_notes = find_xy_wing_cells_in_cells(cells)

  if xywing_cells is None:
    return False

  logger.debug("Isolating notes {} in block for XY-wing in cells: {}".format(xywing_notes, xywing_cells))

  # Remove other notes from xywing cells
  for xywing_cell in xywing_cells:
    for number in range(1, 10):
      if number in xywing_notes:
        continue

      if number not in xywing_cell.notes:
        continue

      xywing_cell.notes.remove(number)
      something_changed = True

  # Remove xywing cell notes from other cells
  for other_cell in cells:
    if other_cell in xywing_cells:
      continue

    for xywing_note in xywing_notes:
      if xywing_note not in other_cell.notes:
        continue
      other_cell.notes.remove(xywing_note)
      something_changed = True

  return something_changed


def handle_xy_wing_in_block(block: list) -> bool:
  cells = block_cells_as_list(block)
  return handle_xy_wing_in_cells(cells)


def solve(sudoku: Sudoku) -> None:
  fill_cellnotes_with_all_values(sudoku)
  eliminate_known_cellnote_values(sudoku)

  while not sudoku.is_solved():
    sudoku.print_extended()
    cell = first_cell_with_one_note(sudoku)

    if cell is not None:
      sudoku.update_cell(cell, cell.notes[0])
      logger.debug("Solving cell {} with single-note-value: {}".format(cell, cell.value))
      continue

    something_changed = False
    for block in sudoku.get_blocks():
      cell, number = single_note_number_in_block(block)

      if cell is None or number is None:
        continue

      logger.debug("Solving cell {} with only-note-in-block-value: {}".format(cell, number))
      sudoku.update_cell(cell, number)
      something_changed = True
    if something_changed:
      continue

    cell, number = single_note_number_in_rows(sudoku)
    if cell is not None and number is not None:
      logger.debug("Solving cell {} with only-note-in-rows-value: {}".format(cell, number))
      sudoku.update_cell(cell, number)
      continue

    cell, number = single_note_number_in_columns(sudoku)
    if cell is not None and number is not None:
      logger.debug("Solving cell {} with only-note-in-columns(-value: {}".format(cell, number))
      sudoku.update_cell(cell, number)
      continue

    for block in sudoku.get_blocks():
      something_changed |= handle_double_notes_in_block(sudoku, block)
    if something_changed:
      continue

    for block in sudoku.get_blocks():
      sudoku.print_extended()
      something_changed |= handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)
    if something_changed:
      continue

    for block in sudoku.get_blocks():
      something_changed |= handle_xy_wing_in_block(block)
    if something_changed:
      continue

    return
