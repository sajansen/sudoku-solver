import logging

from utils import index_of_element_in_2darray, block_cells_as_list

logger = logging.getLogger(__name__)


class Cell:
  def __init__(self, value: int = None, notes: list = None):
    if notes is None:
      notes = []
    else:
      notes.sort()

    self.value = value
    self.notes = notes

  def __str__(self):
    return "Cell(value={}, notes={})".format(self.value, self.notes)

  def __repr__(self):
    return self.__str__()


class Sudoku:
  def __init__(self):
    self.matrix = []
    self.clear()

  def clear(self) -> None:
    self.matrix = []
    for row_index in range(0, 9):
      self.matrix.append([])
      for col_index in range(0, 9):
        self.matrix[row_index].append(Cell())

  def is_solved(self) -> bool:
    for row in self.matrix:
      for cell in row:
        if cell.value is None:
          return False
    return True

  def get_columns(self):
    columns = [[] for i in range(0, 9)]

    for row in self.matrix:
      for column_index in range(0, 9):
        columns[column_index].append(row[column_index])

    return columns

  def get_blocks(self) -> list:
    blocks = [[] for i in range(0, 9)]

    for row_index, row in enumerate(self.matrix):
      block_row_index = 3 * (row_index // 3)

      for block_column_index in range(0, 3):
        cell_index = block_column_index * 3
        blocks[block_row_index + block_column_index].append(row[cell_index:cell_index + 3])
    return blocks

  def get_block_at(self, row_index: int, column_index: int) -> list:
    block_row_index = 3 * (row_index // 3)
    block_column_index = 3 * (column_index // 3)

    block = []
    for row in self.matrix[block_row_index:block_row_index + 3]:
      block.append(row[block_column_index: block_column_index + 3])
    return block

  def remove_notes_from_row(self, row_index: int, notes: list) -> bool:
    notes = list(notes)

    something_changed = False
    for cell in self.matrix[row_index]:
      for note in notes:
        if note not in cell.notes:
          continue
        cell.notes.remove(note)
        something_changed = True
    return something_changed

  def remove_notes_from_column(self, column_index: int, notes: list) -> bool:
    notes = list(notes)

    something_changed = False
    for row in self.matrix:
      cell = row[column_index]
      for note in notes:
        if note not in cell.notes:
          continue
        cell.notes.remove(note)
        something_changed = True
    return something_changed

  def remove_notes_from_block(self, block: list, notes: list) -> bool:
    cells = block_cells_as_list(block)

    something_changed = False
    for cell in cells:
      for note in notes:
        if note not in cell.notes:
          continue
        cell.notes.remove(note)
        something_changed = True
    return something_changed

  def update_cell(self, cell: Cell, value: int):
    row_index, column_index = index_of_element_in_2darray(self.matrix, cell)

    if row_index is None or column_index is None:
      raise IndexError("Can not allocate cell {} in sudoku".format(cell))

    cell.value = value
    cell.notes = []

    self.remove_notes_from_row(row_index, [value])
    self.remove_notes_from_column(column_index, [value])

    block = self.get_block_at(row_index, column_index)
    self.remove_notes_from_block(block, [value])

  def print(self) -> None:
    # |-----------|-----------|-----------|
    # |   . 5 . 2 |   .   .   |   .   .   |
    # | 1 .   . 3 |   .   .   |   .   .   |
    # |   .   .   |   .   .   |   .   .   |
    # |-----------|-----------|-----------|
    # |   . 5 . 2 |   .   .   |   .   .   |
    # | 1 .   . 3 |   .   .   |   .   .   |
    # |   .   .   |   .   .   |   .   .   |
    # |-----------|---+---+---|---+---+---|

    for row_index, row in enumerate(self.matrix):

      if row_index % 3 == 0:
        print("|-----------|-----------|-----------|")

      for cell_index, cell in enumerate(row):
        print("|" if cell_index % 3 == 0 else ".", end='')
        print(" {} ".format(cell.value if cell.value else " "), end='')
      print("|")
    print("|-----------|-----------|-----------|")

  def print_extended(self) -> None:
    logger.info("")
    logger.info(("_" * 14 * 3 + "_____") * 3)
    for row_index, row in enumerate(self.matrix):
      if row_index > 0 and row_index % 3 == 0:
        logger.info(("_" * 14 * 3 + "__|__") * 3)
        logger.info((" " * 14 * 3 + "  |  ") * 3)

      log_string = ""
      for cell_index, cell in enumerate(row):
        if cell.value is not None:
          log_string += " {} ".format(str(cell.value).center(12))
        else:
          log_string += " {:12s} ".format("[" + ','.join([str(note) for note in cell.notes]) + "]")

        if cell_index % 3 == 2:
          log_string += "  |  "
      logger.info(log_string)
    logger.info("")
