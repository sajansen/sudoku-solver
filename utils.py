def index_of_element_in_2darray(array: list, element) -> (int, int):
  for row_index, row in enumerate(array):
    try:
      element_index = row.index(element)
      return row_index, element_index
    except ValueError:
      continue
  return None, None


def block_cells_as_list(block: list) -> list:
  return [cell for row in block for cell in row]
