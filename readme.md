# sudoku-solver

```
============   SOLUTION   ===========

|-----------|-----------|-----------|
| 5 . 7 . 6 | 2 . 8 . 3 | 1 . 4 . 9 |
| 1 . 9 . 8 | 6 . 4 . 5 | 7 . 2 . 3 |
| 4 . 2 . 3 | 7 . 9 . 1 | 6 . 8 . 5 |
|-----------|-----------|-----------|
| 9 . 5 . 2 | 3 . 7 . 8 | 4 . 1 . 6 |
| 8 . 3 . 7 | 1 . 6 . 4 | 5 . 9 . 2 |
| 6 . 4 . 1 | 5 . 2 . 9 | 8 . 3 . 7 |
|-----------|-----------|-----------|
| 2 . 1 . 5 | 4 . 3 . 6 | 9 . 7 . 8 |
| 3 . 8 . 4 | 9 . 5 . 7 | 2 . 6 . 1 |
| 7 . 6 . 9 | 8 . 1 . 2 | 3 . 5 . 4 |
|-----------|-----------|-----------|

=====================================
```

## Usage

1. Run main.py: `python main.py`
1. Insert each row of the sudoku and confirm the row with [Enter].
1. After confirming the final row, the initial sudoku and it's solution will be printed. If no solution is found (because of the lack of advanced solution tactics in the code), the latest state is printed out.


#### Input format

Insert the numbers of each line using the following format:
```
123456789       # For a row | 1 . 2 . 3 | 4 . 5 . 6 | 7 . 8 . 9 |
```

For empty cells, you can use either a zero (`0`) or a whitespace (` `):
```
1  45 7         # For a row | 1 .   .   | 4 . 5 .   | 7 .   .   |
100450700       # For a row | 1 .   .   | 4 . 5 .   | 7 .   .   |
``` 

A uncompleted row will result in the last columns being interpreted as empty cells:
```
10005           # For a row | 1 .   .   |   . 5 .   |   .   .   |
``` 

For readability, you can use dots (`.`) and/or `|` wherever you want. I'd recommend to use them for seperating the blocks, like so:
```
123.456.789       # For a row | 1 . 2 . 3 | 4 . 5 . 6 | 7 . 8 . 9 |
123|456|789       # For a row | 1 . 2 . 3 | 4 . 5 . 6 | 7 . 8 . 9 |
``` 

You can also generate your sudoku somewhere else, and than just copy and paste it into the first program input prompt. The generated lines must al be terminated with a [Enter]. Example:
```
070.000.049
190.005.700
000.001.600
052.300.000
800.060.002
000.009.830
005.400.000
004.900.061
760.000.050
```

This will be interpreted as: 
```
|-----------|-----------|-----------|
|   . 7 .   |   .   .   |   . 4 . 9 |
| 1 . 9 .   |   .   . 5 | 7 .   .   |
|   .   .   |   .   . 1 | 6 .   .   |
|-----------|-----------|-----------|
|   . 5 . 2 | 3 .   .   |   .   .   |
| 8 .   .   |   . 6 .   |   .   . 2 |
|   .   .   |   .   . 9 | 8 . 3 .   |
|-----------|-----------|-----------|
|   .   . 5 | 4 .   .   |   .   .   |
|   .   . 4 | 9 .   .   |   . 6 . 1 |
| 7 . 6 .   |   .   .   |   . 5 .   |
|-----------|-----------|-----------|
```

## Development

The solving process is done in [solver.py](solver.py) in the `solve(sudoku: Sudoku)` function. Here, more solving tactics can be added. This function will continue trying to solve the sudoku until the sudoku is copmlete solved or no changes are made using the known tactics. 

