import unittest

from sudoku import Sudoku, Cell


class SudokuTestCase(unittest.TestCase):

  def test_sudoku_clear(self):
    sudoku = Sudoku()
    sudoku.matrix.append(Cell(1))
    sudoku.clear()

    self.assertEqual(9, len(sudoku.matrix))
    self.assertEqual(9, len(sudoku.matrix[0]))
    self.assertEqual(9, len(sudoku.matrix[1]))
    self.assertEqual(9, len(sudoku.matrix[2]))
    self.assertEqual(9, len(sudoku.matrix[7]))
    self.assertEqual(9, len(sudoku.matrix[8]))
    self.assertTrue(isinstance(sudoku.matrix[0][0], Cell))
    self.assertTrue(isinstance(sudoku.matrix[0][1], Cell))
    self.assertTrue(isinstance(sudoku.matrix[0][8], Cell))
    self.assertTrue(isinstance(sudoku.matrix[8][0], Cell))
    self.assertTrue(isinstance(sudoku.matrix[8][8], Cell))
    self.assertIsNone(sudoku.matrix[0][0].value)
    self.assertIsNone(sudoku.matrix[0][1].value)
    self.assertIsNone(sudoku.matrix[0][8].value)
    self.assertIsNone(sudoku.matrix[8][0].value)
    self.assertIsNone(sudoku.matrix[8][8].value)

  def test_sudoku_is_solved(self):
    sudoku = Sudoku()
    for row in sudoku.matrix:
      for cell in row:
        self.assertFalse(sudoku.is_solved())
        cell.value = 1

    self.assertTrue(sudoku.is_solved())

  def test_sudoku_get_columns(self):
    sudoku = Sudoku()

    columns = sudoku.get_columns()

    self.assertEqual(sudoku.matrix[0][0], columns[0][0])
    self.assertEqual(sudoku.matrix[0][1], columns[1][0])
    self.assertEqual(sudoku.matrix[0][2], columns[2][0])
    self.assertEqual(sudoku.matrix[2][0], columns[0][2])
    self.assertEqual(sudoku.matrix[2][8], columns[8][2])
    self.assertEqual(sudoku.matrix[8][8], columns[8][8])

  def test_sudoku_get_blocks(self):
    sudoku = Sudoku()
    blocks = sudoku.get_blocks()

    self.assertEqual(3, len(blocks[0]))
    self.assertEqual(sudoku.matrix[0][0], blocks[0][0][0])
    self.assertEqual(sudoku.matrix[2][2], blocks[0][2][2])
    self.assertEqual(sudoku.matrix[0][3], blocks[1][0][0])
    self.assertEqual(sudoku.matrix[0][6], blocks[2][0][0])
    self.assertEqual(sudoku.matrix[3][0], blocks[3][0][0])
    self.assertEqual(sudoku.matrix[5][5], blocks[4][2][2])
    self.assertEqual(sudoku.matrix[6][6], blocks[8][0][0])

  def test_sudoku_get_block_at(self):
    sudoku = Sudoku()
    block = sudoku.get_block_at(3, 4)

    self.assertEqual(3, len(block))
    self.assertEqual(sudoku.matrix[3][3], block[0][0])
    self.assertEqual(sudoku.matrix[5][5], block[2][2])


  def test_sudoku_remove_notes_from_row(self):
    sudoku = Sudoku()
    sudoku.matrix[1][0].notes = [1, 2, 3]
    sudoku.matrix[1][5].notes = [1, 2, 3]
    sudoku.matrix[0][1].notes = [1, 2, 3]

    something_changed = sudoku.remove_notes_from_row(1, [1, 3])

    self.assertTrue(something_changed)
    self.assertEqual([2], sudoku.matrix[1][0].notes)
    self.assertEqual([2], sudoku.matrix[1][5].notes)
    self.assertEqual([1, 2, 3], sudoku.matrix[0][1].notes)

    # Re-executing won't result in a change
    something_changed = sudoku.remove_notes_from_row(1, [1, 3])
    self.assertFalse(something_changed)

  def test_sudoku_remove_notes_from_column(self):
    sudoku = Sudoku()
    sudoku.matrix[0][1].notes = [1, 2, 3]
    sudoku.matrix[5][1].notes = [1, 2, 3]
    sudoku.matrix[1][0].notes = [1, 2, 3]

    something_changed = sudoku.remove_notes_from_column(1, [1, 3])

    self.assertTrue(something_changed)
    self.assertEqual([2], sudoku.matrix[0][1].notes)
    self.assertEqual([2], sudoku.matrix[5][1].notes)
    self.assertEqual([1, 2, 3], sudoku.matrix[1][0].notes)

    # Re-executing won't result in a change
    something_changed = sudoku.remove_notes_from_column(1, [1, 3])
    self.assertFalse(something_changed)

  def test_sudoku_update_cell(self):
    sudoku = Sudoku()
    update_cell = Cell(notes=[2, 3])
    sudoku.matrix[1][1] = update_cell
    sudoku.matrix[0][0].notes = [1, 2, 3, 4]
    sudoku.matrix[1][0].notes = [1, 2, 3, 4]
    sudoku.matrix[1][5].notes = [1, 2, 3]
    sudoku.matrix[0][1].notes = [1, 3, 5]
    sudoku.matrix[5][1].notes = [1, 3]

    sudoku.update_cell(update_cell, 3)

    self.assertEqual(3, sudoku.matrix[1][1].value)
    self.assertEqual([], sudoku.matrix[1][1].notes)
    self.assertEqual([1, 2, 4], sudoku.matrix[0][0].notes)
    self.assertEqual([1, 2, 4], sudoku.matrix[1][0].notes)
    self.assertEqual([1, 2], sudoku.matrix[1][5].notes)
    self.assertEqual([1, 5], sudoku.matrix[0][1].notes)
    self.assertEqual([1], sudoku.matrix[5][1].notes)
