import logging
import unittest

import main
import solver
from sudoku import Sudoku, Cell
from utils import block_cells_as_list

logger = logging.getLogger(__name__)
logging = logging.basicConfig(level=logging.DEBUG)


class SolverTest(unittest.TestCase):
  def setUp(self) -> None:
    sudoku_matrix = [
      main.string_to_int_list('  34 9   '),
      main.string_to_int_list('1   5  4 '),
      main.string_to_int_list('        3'),
      main.string_to_int_list(' 2 7    8'),
    ]
    self.sudoku = Sudoku()
    main.init_sudoku_with_values(self.sudoku, sudoku_matrix)

  def test_fill_cellnotes_with_all_values(self):
    solver.fill_cellnotes_with_all_values(self.sudoku)

    self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sudoku.matrix[0][0].notes)
    self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], self.sudoku.matrix[3][3].notes)

  def test_existing_cells_in_block(self):
    self.assertEqual([1, 3], solver.existing_cells_in_block(self.sudoku, 1, 1))
    self.assertEqual([1, 3], solver.existing_cells_in_block(self.sudoku, 2, 0))
    self.assertEqual([4, 5, 9], solver.existing_cells_in_block(self.sudoku, 0, 3))
    self.assertEqual([], solver.existing_cells_in_block(self.sudoku, 8, 8))

  def test_existing_cells_in_row(self):
    self.assertEqual([3, 4, 9], solver.existing_cells_in_row(self.sudoku, 0))
    self.assertEqual([3], solver.existing_cells_in_row(self.sudoku, 2))
    self.assertEqual([], solver.existing_cells_in_row(self.sudoku, 8))

  def test_existing_cells_in_column(self):
    self.assertEqual([1], solver.existing_cells_in_column(self.sudoku, 0))
    self.assertEqual([4, 7], solver.existing_cells_in_column(self.sudoku, 3))

  def test_eliminate_known_cellnote_values(self):
    solver.fill_cellnotes_with_all_values(self.sudoku)
    solver.eliminate_known_cellnote_values(self.sudoku)

    self.assertEqual([2, 5, 6, 7, 8], self.sudoku.matrix[0][0].notes)
    self.assertEqual([5, 6, 7, 8], self.sudoku.matrix[0][1].notes)
    self.assertEqual([], self.sudoku.matrix[1][0].notes)

  def test_block_cells_as_list(self):
    block = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]

    self.assertEqual([1, 2, 3, 4, 5, 6, 7, 8, 9], solver.block_cells_as_list(block))

  def test_single_note_number_in_cell_list(self):
    expected_cell = Cell(notes=[2, 5])
    cell_list = [Cell(value=1), Cell(notes=[2, 3, 4]), expected_cell, Cell(notes=[3, 4])]
    found_cell, number = solver.single_note_number_in_cell_list(cell_list)

    self.assertEqual(expected_cell, found_cell)
    self.assertEqual(5, number)

  def test_single_note_number_in_block(self):
    expected_cell = Cell(notes=[2, 4, 5])
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), Cell(notes=[3, 4])],
      [Cell(notes=[2]), Cell(value=1), Cell(notes=[3, 4])],
      [Cell(value=1), expected_cell, Cell(notes=[3, 4])],
    ]

    cell, number = solver.single_note_number_in_block(block)

    self.assertEqual(expected_cell, cell)
    self.assertEqual(5, number)

  def test_single_note_number_in_block_2(self):
    expected_cell = Cell(value=None, notes=[3, 4, 7, 8, 9])
    block = [
      [Cell(value=None, notes=[2, 6, 8]), Cell(value=None, notes=[3, 8]), Cell(value=None, notes=[2, 3, 8])],
      [Cell(value=None, notes=[2, 6, 8]), Cell(value=None, notes=[4, 8]), Cell(value=5, notes=[])],
      [Cell(value=None, notes=[2, 7, 8]), expected_cell, Cell(value=1, notes=[])]
    ]

    cell, number = solver.single_note_number_in_block(block)

    self.assertEqual(expected_cell, cell)
    self.assertEqual(9, number)

  def test_single_note_number_in_block_but_not_found(self):
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), Cell(notes=[3, 4])],
      [Cell(notes=[2]), Cell(value=1), Cell(notes=[3, 4])],
      [Cell(value=1), Cell(notes=[2, 3]), Cell(notes=[3, 4])],
    ]

    cell, number = solver.single_note_number_in_block(block)

    self.assertIsNone(cell)
    self.assertIsNone(number)

  def test_single_note_number_in_rows(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[1][0].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][2].notes = [1, 2, 3, 4, 5, 6]
    sudoku.matrix[1][3].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][4].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][5].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][6].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][7].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][8].notes = [1, 2, 3, 4, 5]

    found_cell, number = solver.single_note_number_in_rows(sudoku)

    self.assertEqual(sudoku.matrix[1][2], found_cell)
    self.assertEqual(6, number)

  def test_single_note_number_in_columns(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[0][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[1][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[2][1].notes = [1, 2, 3, 4, 5, 6]
    sudoku.matrix[3][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[4][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[5][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[6][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[7][1].notes = [1, 2, 3, 4, 5]
    sudoku.matrix[8][1].notes = [1, 2, 3, 4, 5]

    found_cell, number = solver.single_note_number_in_columns(sudoku)

    self.assertEqual(sudoku.matrix[2][1], found_cell)
    self.assertEqual(6, number)

  def test_find_double_notes_in_block(self):
    expected_first_cell = Cell(notes=[3, 4])
    expected_second_cell = Cell(notes=[3, 4])
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), expected_first_cell],
      [Cell(notes=[2]), Cell(value=1), expected_second_cell],
      [Cell(value=1), Cell(notes=[2, 3]), Cell(notes=[5, 7])],
    ]

    first_cell, second_cell = solver.find_double_notes_in_block(block)

    self.assertEqual(expected_first_cell, first_cell)
    self.assertEqual(expected_second_cell, second_cell)

  def test_find_double_notes_in_block_2(self):
    expected_first_cell = Cell(notes=[7, 9])
    expected_second_cell = Cell(notes=[7, 9])
    block = [
      [Cell(notes=[2, 3, 9]), expected_first_cell, expected_second_cell],
      [Cell(notes=[2, 3]), Cell(value=1), Cell(value=1)],
      [Cell(notes=[2, 3, 4, 9]), Cell(value=1), Cell(notes=[4, 8])],
    ]

    first_cell, second_cell = solver.find_double_notes_in_block(block)

    self.assertEqual(expected_first_cell, first_cell)
    self.assertEqual(expected_second_cell, second_cell)

  def test_find_double_notes_in_block_where_is_none(self):
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), Cell(notes=[3, 4])],
      [Cell(notes=[2]), Cell(value=1), Cell(notes=[3, 7])],
      [Cell(value=1), Cell(notes=[2, 3]), Cell(notes=[5, 7])],
    ]

    first_cell, second_cell = solver.find_double_notes_in_block(block)

    self.assertIsNone(first_cell)
    self.assertIsNone(second_cell)

  def test_handle_double_notes_in_block(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[1][4].notes = [2, 8]
    sudoku.matrix[2][4].notes = [2, 8]

    block = sudoku.get_blocks()[1]
    result = solver.handle_double_notes_in_block(sudoku, block)

    self.assertTrue(result)
    self.assertEqual([2, 8], sudoku.matrix[1][4].notes)
    self.assertEqual([2, 8], sudoku.matrix[2][4].notes)
    self.assertFalse(2 in sudoku.matrix[0][3].notes)
    self.assertFalse(8 in sudoku.matrix[0][3].notes)
    self.assertFalse(2 in sudoku.matrix[5][4].notes)
    self.assertFalse(8 in sudoku.matrix[5][4].notes)
    self.assertTrue(2 in sudoku.matrix[1][8].notes)
    self.assertTrue(8 in sudoku.matrix[1][8].notes)
    self.assertTrue(2 in sudoku.matrix[2][8].notes)
    self.assertTrue(8 in sudoku.matrix[2][8].notes)

    # Second run won't trigger on the same cells
    result = solver.handle_double_notes_in_block(sudoku, block)
    self.assertFalse(result)

  def test_handle_double_notes_in_block_2(self):
    sudoku = Sudoku()
    sudoku.matrix[3][6].notes = [4, 9]
    sudoku.matrix[6][4].notes = [1, 3, 7, 8]
    sudoku.matrix[6][6].notes = [2, 3, 8, 9]
    sudoku.matrix[6][7].notes = [7, 9]
    sudoku.matrix[6][8].notes = [7, 9]
    sudoku.matrix[7][6].notes = [2, 3]
    sudoku.matrix[8][6].notes = [2, 3, 4, 9]
    sudoku.matrix[8][8].notes = [4, 8]

    block = sudoku.get_blocks()[8]
    result = solver.handle_double_notes_in_block(sudoku, block)

    self.assertTrue(result)
    self.assertEqual([4, 9], sudoku.matrix[3][6].notes)
    self.assertEqual([1, 3, 8], sudoku.matrix[6][4].notes)
    self.assertEqual([2, 3, 8], sudoku.matrix[6][6].notes)
    self.assertEqual([7, 9], sudoku.matrix[6][7].notes)
    self.assertEqual([7, 9], sudoku.matrix[6][8].notes)
    self.assertEqual([2, 3], sudoku.matrix[7][6].notes)
    self.assertEqual([2, 3, 4], sudoku.matrix[8][6].notes)
    self.assertEqual([4, 8], sudoku.matrix[8][8].notes)

    result = solver.handle_double_notes_in_block(sudoku, block)
    self.assertFalse(result)

  def test_handle_double_notes_in_block_without_double_notes(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[1][1].notes = [2, 8]
    sudoku.matrix[2][1].notes = [2, 7]

    block = sudoku.get_blocks()[0]
    result = solver.handle_double_notes_in_block(sudoku, block)

    self.assertFalse(result)
    self.assertEqual([2, 8], sudoku.matrix[1][1].notes)
    self.assertEqual([2, 7], sudoku.matrix[2][1].notes)
    self.assertTrue(2 in sudoku.matrix[0][0].notes)
    self.assertTrue(8 in sudoku.matrix[0][0].notes)
    self.assertTrue(2 in sudoku.matrix[5][1].notes)
    self.assertTrue(8 in sudoku.matrix[5][1].notes)
    self.assertTrue(2 in sudoku.matrix[1][5].notes)
    self.assertTrue(8 in sudoku.matrix[1][5].notes)
    self.assertTrue(2 in sudoku.matrix[2][5].notes)
    self.assertTrue(8 in sudoku.matrix[2][5].notes)

  def test_find_double_note_number_in_block(self):
    expected_first_cell = Cell(notes=[3, 8])
    expected_second_cell = Cell(notes=[3, 4, 8])
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), expected_first_cell],
      [Cell(notes=[2, 5, 7]), Cell(value=1), expected_second_cell],
      [Cell(value=1), Cell(notes=[2, 3]), Cell(notes=[5, 7])],
    ]

    found_cells, row_column = solver.find_double_note_number_in_block(block, 8)

    self.assertEqual(2, len(found_cells))
    self.assertTrue(expected_first_cell in found_cells)
    self.assertTrue(expected_second_cell in found_cells)
    self.assertIsNone(row_column[0])
    self.assertEqual(2, row_column[1])

  def test_find_double_note_number_in_block_2(self):
    expected_first_cell = Cell(notes=[7, 9])
    expected_second_cell = Cell(notes=[7, 8])
    block = [
      [Cell(notes=[2, 3, 9]), expected_first_cell, expected_second_cell],
      [Cell(notes=[2, 3]), Cell(value=1), Cell(value=1)],
      [Cell(notes=[2, 3, 4, 9]), Cell(value=1), Cell(notes=[4, 8])],
    ]

    found_cells, row_column = solver.find_double_note_number_in_block(block, 7)

    self.assertEqual(2, len(found_cells))
    self.assertTrue(expected_first_cell in found_cells)
    self.assertTrue(expected_second_cell in found_cells)
    self.assertEqual(0, row_column[0])
    self.assertIsNone(row_column[1])

  def test_find_double_note_number_in_block_without_matches(self):
    block = [
      [Cell(value=1), Cell(notes=[2, 3, 4]), Cell(notes=[3, 4])],
      [Cell(notes=[2, 5, 7, 4]), Cell(value=1), Cell(notes=[3, 7])],
      [Cell(value=1), Cell(notes=[2, 3]), Cell(notes=[5, 7])],
    ]

    found_cells, row_column = solver.find_double_note_number_in_block(block, 7)

    self.assertIsNone(found_cells)
    self.assertIsNone(row_column)

  def test_handle_double_note_number_in_block_and_row_or_column(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[0][3].notes = [2, 7, 8]
    sudoku.matrix[1][3].notes = [2, 7, 8]
    sudoku.matrix[2][3].notes = [2, 7, 8]
    sudoku.matrix[0][4].notes = [7, 8]
    sudoku.matrix[1][4].notes = [7, 8]
    sudoku.matrix[2][4].notes = [7, 8]
    sudoku.matrix[0][5].notes = [7, 8]
    sudoku.matrix[1][5].notes = [7, 8]
    sudoku.matrix[2][5].notes = [7, 8]

    block = sudoku.get_blocks()[1]
    result = solver.handle_double_note_number_in_block_and_row_or_column(sudoku, block, 2)

    self.assertTrue(result)
    self.assertEqual([2, 7, 8], sudoku.matrix[0][3].notes)
    self.assertEqual([2, 7, 8], sudoku.matrix[1][3].notes)
    self.assertEqual([2, 7, 8], sudoku.matrix[2][3].notes)
    self.assertFalse(2 in sudoku.matrix[3][3].notes)
    self.assertFalse(2 in sudoku.matrix[8][3].notes)

    result = solver.handle_double_note_number_in_block_and_row_or_column(sudoku, block, 2)
    self.assertFalse(result)

  def test_handle_all_double_note_numbers_in_block_and_row_or_column(self):
    sudoku = Sudoku()
    solver.fill_cellnotes_with_all_values(sudoku)
    sudoku.matrix[0][3].notes = [2, 7, 8]
    sudoku.matrix[1][3].notes = [2, 7, 8]
    sudoku.matrix[2][3].notes = [2, 7, 8]
    sudoku.matrix[0][4].notes = [7, 8]
    sudoku.matrix[1][4].notes = [7, 8]
    sudoku.matrix[2][4].notes = [7, 8]
    sudoku.matrix[0][5].notes = [7, 8]
    sudoku.matrix[1][5].notes = [7, 8]
    sudoku.matrix[2][5].notes = [7, 8]

    block = sudoku.get_blocks()[1]
    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)

    self.assertTrue(result)
    self.assertEqual([2, 7, 8], sudoku.matrix[0][3].notes)
    self.assertEqual([2, 7, 8], sudoku.matrix[1][3].notes)
    self.assertEqual([2, 7, 8], sudoku.matrix[2][3].notes)
    self.assertFalse(2 in sudoku.matrix[3][3].notes)
    self.assertFalse(2 in sudoku.matrix[8][3].notes)

    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)
    self.assertFalse(result)

  def test_handle_all_double_note_numbers_in_block_and_row_or_column_2(self):
    sudoku = Sudoku()
    sudoku.matrix[2][0].notes = [2, 3, 4]
    sudoku.matrix[2][1].notes = [2, 3, 4, 8]
    sudoku.matrix[0][2].notes = [3, 6, 8]
    sudoku.matrix[1][2].notes = [6, 8]
    sudoku.matrix[2][2].notes = [3, 8]
    sudoku.matrix[0][3].notes = [2, 6, 8]
    sudoku.matrix[1][3].notes = [2, 6, 8]
    sudoku.matrix[0][4].notes = [3, 8]
    sudoku.matrix[0][5].notes = [2, 3, 8]
    sudoku.matrix[1][7].notes = [2, 8]
    sudoku.matrix[2][7].notes = [2, 8]

    block = sudoku.get_blocks()[0]
    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)

    self.assertTrue(result)
    self.assertEqual([2, 3, 4], sudoku.matrix[2][0].notes)
    self.assertEqual([2, 3, 4, 8], sudoku.matrix[2][1].notes)
    self.assertEqual([3, 6, 8], sudoku.matrix[0][2].notes)
    self.assertEqual([6, 8], sudoku.matrix[1][2].notes)
    self.assertEqual([3, 8], sudoku.matrix[2][2].notes)
    self.assertEqual([2, 8], sudoku.matrix[1][7].notes)
    self.assertEqual([8], sudoku.matrix[2][7].notes)

    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)
    self.assertFalse(result)

  def test_handle_all_double_note_numbers_in_block_and_row_or_column_3(self):
    sudoku = Sudoku()
    sudoku.matrix[3][6].notes = [4, 9]
    sudoku.matrix[6][4].notes = [1, 3, 7, 8]
    sudoku.matrix[6][6].notes = [2, 3, 9]
    sudoku.matrix[6][7].notes = [7, 9]
    sudoku.matrix[6][8].notes = [7, 8]
    sudoku.matrix[7][6].notes = [2, 3]
    sudoku.matrix[8][6].notes = [2, 3, 4, 9]
    sudoku.matrix[8][8].notes = [4, 8]

    block = sudoku.get_blocks()[8]
    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)

    self.assertTrue(result)
    self.assertEqual([4, 9], sudoku.matrix[3][6].notes)
    self.assertEqual([1, 3, 8], sudoku.matrix[6][4].notes)
    self.assertEqual([2, 3, 9], sudoku.matrix[6][6].notes)
    self.assertEqual([7, 9], sudoku.matrix[6][7].notes)
    self.assertEqual([7, 8], sudoku.matrix[6][8].notes)
    self.assertEqual([2, 3], sudoku.matrix[7][6].notes)
    self.assertEqual([2, 3, 4, 9], sudoku.matrix[8][6].notes)
    self.assertEqual([4, 8], sudoku.matrix[8][8].notes)

    result = solver.handle_all_double_note_numbers_in_block_and_row_or_column(sudoku, block)
    self.assertFalse(result)

  def test_find_xy_wing_cells_in_cells(self):
    expected_first_cell = Cell(notes=[4, 9])
    expected_second_cell = Cell(notes=[7, 9])
    expected_third_cell = Cell(notes=[4, 6, 7])
    block = [
      [expected_first_cell, Cell(value=1), Cell(notes=[4, 6])],
      [Cell(value=5), expected_second_cell, Cell(value=2)],
      [Cell(value=8), Cell(value=3), expected_third_cell],
    ]

    cells = block_cells_as_list(block)
    xywing_cells, xywing_notes = solver.find_xy_wing_cells_in_cells(cells)

    self.assertEqual([expected_first_cell, expected_second_cell, expected_third_cell], xywing_cells)
    self.assertEqual([4, 7, 9], xywing_notes)

  def test_find_xy_wing_cells_in_cells_2(self):
    expected_first_cell = Cell(notes=[3, 8])
    expected_second_cell = Cell(notes=[2, 3])
    expected_third_cell = Cell(notes=[2, 6, 8])
    block = [
      [expected_third_cell, expected_first_cell, expected_second_cell],
      [Cell(notes=[6, 8]), Cell(value=4), Cell(value=5)],
      [Cell(value=7), Cell(value=9), Cell(value=1)],
    ]

    cells = block_cells_as_list(block)
    xywing_cells, xywing_notes = solver.find_xy_wing_cells_in_cells(cells)

    self.assertEqual([expected_first_cell, expected_second_cell, expected_third_cell], xywing_cells)
    self.assertEqual([2, 3, 8], xywing_notes)

  def test_handle_xy_wing_in_block(self):
    block = [
      [Cell(notes=[4, 9]), Cell(value=1), Cell(notes=[4, 6])],
      [Cell(value=5), Cell(notes=[7, 9]), Cell(value=2)],
      [Cell(value=8), Cell(value=3), Cell(notes=[4, 6, 7])],
    ]

    something_changed = solver.handle_xy_wing_in_block(block)

    self.assertTrue(something_changed)
    self.assertEqual([4, 9], block[0][0].notes)
    self.assertEqual([7, 9], block[1][1].notes)
    self.assertEqual([4, 7], block[2][2].notes)
    self.assertEqual([6], block[0][2].notes)

    something_changed = solver.handle_xy_wing_in_block(block)
    self.assertFalse(something_changed)

  def test_handle_xy_wing_in_block(self):
    block = [
      [Cell(notes=[2, 6, 8]), Cell(notes=[3, 8]), Cell(notes=[2, 3])],
      [Cell(notes=[6, 8]), Cell(value=4), Cell(value=5)],
      [Cell(value=7), Cell(value=9), Cell(value=1)],
    ]

    something_changed = solver.handle_xy_wing_in_block(block)

    self.assertTrue(something_changed)
    self.assertEqual([2, 8], block[0][0].notes)
    self.assertEqual([3, 8], block[0][1].notes)
    self.assertEqual([2, 3], block[0][2].notes)
    self.assertEqual([6], block[1][0].notes)

    something_changed = solver.handle_xy_wing_in_block(block)
    self.assertFalse(something_changed)

  def test_solver(self):
    sudoku_matrix = [
      main.string_to_int_list('  9.8  .  6'),
      main.string_to_int_list('   .92 . 4 '),
      main.string_to_int_list(' 5 .4 3.  1'),
      main.string_to_int_list('   .   .6 7'),
      main.string_to_int_list('  3.1 7.   '),
      main.string_to_int_list('  7. 68.9  '),
      main.string_to_int_list('1  .6 5.47 '),
      main.string_to_int_list('   .  4.  2'),
      main.string_to_int_list('7  . 1 .53 '),
    ]
    sudoku = Sudoku()
    main.init_sudoku_with_values(sudoku, sudoku_matrix)

    solver.solve(sudoku)

    self.assertTrue(sudoku.is_solved())
    self.assertEqual(3, sudoku.matrix[0][0].value)
    self.assertEqual(4, sudoku.matrix[8][2].value)
    self.assertEqual(8, sudoku.matrix[7][2].value)
    self.assertEqual(3, sudoku.matrix[7][4].value)

  def test_solver_2(self):
    sudoku_matrix = [
      main.string_to_int_list('   .41 .8  '),
      main.string_to_int_list(' 8 .  3.   '),
      main.string_to_int_list('  4.   .156'),
      main.string_to_int_list('  6.   .2  '),
      main.string_to_int_list('2  .3  .  4'),
      main.string_to_int_list('7 3.   . 1 '),
      main.string_to_int_list('   . 4 .   '),
      main.string_to_int_list('5 1. 7 . 98'),
      main.string_to_int_list('6  .  1.   '),
    ]
    sudoku = Sudoku()
    main.init_sudoku_with_values(sudoku, sudoku_matrix)

    solver.solve(sudoku)

    self.assertTrue(sudoku.is_solved())
    self.assertEqual(9, sudoku.matrix[0][0].value)
    self.assertEqual(5, sudoku.matrix[8][8].value)
    self.assertEqual(2, sudoku.matrix[8][7].value)
    self.assertEqual(9, sudoku.matrix[4][4].value)

  # Extreme puzzle
  def test_solver_3(self):
    sudoku_matrix = [
      main.string_to_int_list(' 7 .   . 49'),
      main.string_to_int_list('19 .  5.7  '),
      main.string_to_int_list('   .  1.6  '),
      main.string_to_int_list(' 52.3  .   '),
      main.string_to_int_list('8  . 6 .  2'),
      main.string_to_int_list('   .  9.83 '),
      main.string_to_int_list('  5.4  .   '),
      main.string_to_int_list('  4.9  . 61'),
      main.string_to_int_list('76 .   . 5 '),
    ]
    sudoku = Sudoku()
    main.init_sudoku_with_values(sudoku, sudoku_matrix)

    solver.solve(sudoku)

    self.assertTrue(sudoku.is_solved())
    self.assertEqual(5, sudoku.matrix[0][0].value)
    self.assertEqual(7, sudoku.matrix[4][2].value)
    self.assertEqual(3, sudoku.matrix[7][0].value)
    self.assertEqual(1, sudoku.matrix[5][2].value)
    self.assertEqual(4, sudoku.matrix[8][8].value)

  # Extreme puzzle
  def test_solver_4(self):
    sudoku_matrix = [
      main.string_to_int_list('100.000.006'),
      main.string_to_int_list('073.000.090'),
      main.string_to_int_list('004.500.720'),
      main.string_to_int_list('000.010.400'),
      main.string_to_int_list('000.835.000'),
      main.string_to_int_list('006.020.000'),
      main.string_to_int_list('039.006.800'),
      main.string_to_int_list('050.000.970'),
      main.string_to_int_list('400.000.003'),
    ]
    sudoku = Sudoku()
    main.init_sudoku_with_values(sudoku, sudoku_matrix)

    solver.solve(sudoku)

    self.assertTrue(sudoku.is_solved())
    self.assertEqual(9, sudoku.matrix[0][1].value)
    self.assertEqual(9, sudoku.matrix[3][5].value)
    self.assertEqual(3, sudoku.matrix[3][0].value)
    self.assertEqual(8, sudoku.matrix[7][2].value)
    self.assertEqual(8, sudoku.matrix[8][5].value)
