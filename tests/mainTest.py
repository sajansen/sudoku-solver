import unittest

import main
from sudoku import Sudoku, Cell


class mainTest(unittest.TestCase):

  def test_init_sudoku_with_values(self):
    sudoku = main.Sudoku()
    value_matrix = [
      [1, None, None, None, None, None, None, None, None],
      [None, 2, None, None, None, None, None, None, None],
      [None, 2, 3, None, None, None, None, None, None],
      [None, None, 4, 5, None, None, None, None, None],
    ]

    main.init_sudoku_with_values(sudoku, value_matrix)

    self.assertEqual(1, sudoku.matrix[0][0].value)
    self.assertEqual(2, sudoku.matrix[1][1].value)
    self.assertEqual(3, sudoku.matrix[2][2].value)
    self.assertEqual(4, sudoku.matrix[3][2].value)
    self.assertEqual(5, sudoku.matrix[3][3].value)
    self.assertIsNone(sudoku.matrix[0][1].value)
    self.assertIsNone(sudoku.matrix[1][0].value)
    self.assertIsNone(sudoku.matrix[1][2].value)
    self.assertIsNone(sudoku.matrix[4][0].value)

  def test_char_list_to_int_list(self):
    input_list = ['0', '1', ' ', 'x', 3]
    output_list = main.char_list_to_int_list(input_list)

    self.assertEqual(9, len(output_list))
    self.assertEqual(0, output_list[0])
    self.assertEqual(1, output_list[1])
    self.assertIsNone(output_list[2])
    self.assertIsNone(output_list[3])
    self.assertEqual(3, output_list[4])
    self.assertIsNone(output_list[5])

  def test_string_to_int_list(self):
    self.assertEqual([1, 2, None, None, 3, 4, 5, None, None], main.string_to_int_list("120. 345"))
